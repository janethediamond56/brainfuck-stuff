# Written by Joe Diamond, jdiamond_11@comcast.net
# Written with Python version 3.6.2
# To use, call the command
# "python compiler.py <source> -o <outputfile>" without quotes
# WARNING: Comments in the source file WILL BE DELETED.
import sys,os
file = open(sys.argv[1],'r')
file2 = open(sys.argv[1]+".min","w")
while True:
    char = file.read(1)
    if not char:
        break
    elif char == "+":
        file2.write(char)
    elif char == "-":
        file2.write(char)
    elif char == "[":
        file2.write(char)
    elif char == "]":
        file2.write(char)
    elif char == "<":
        file2.write(char)
    elif char == ">":
        file2.write(char)
    elif char == ".":
        file2.write(char)
    elif char == ",":
        file2.write(char)
    else:
        pass
file2.close()
file.close()
file2 = open(sys.argv[1]+".min","r")
file1 = open(sys.argv[1],"w")
while True:
    char = file2.read()
    if not char:
        break
    file1.write(char)
file1.close()
file2.close()
os.remove(sys.argv[1]+".min")
    